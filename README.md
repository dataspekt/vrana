# Hydrological and related data from lake Vrana on the island of Cres (Croatia)

This is a **work-in-progress** attempt to collect and consolidate data from multiple sources. Data are consolidated on three aggregation levels: hourly, daily and monthly. Processed datasets of individual indicators and consolidated datasets are located in `data/` directory and source datasets are located in its subdirectories. Processed datasets are available in RDS format and consolidated datasets are also available in CSV format.

## Consolidated datasets

Consolidated datasets come in three versions: 

* full version (`data_*_full`) containing all data for every indicator available on that level of aggregation, in long form, including duplicated values for time points from different sources; 
* sourced version (`data_*_sourced`) containing a single value of indicator per time point, in long form, where its source is referenced in column `data_source`; 
* unsourced version (`data_*`) in wide form.

Aggregated data is used in consolidation, for example if data exist on daily level then consolidated daily data will be used via aggregation as input for consolidating monthly data. In such cases sources are not explicit (referenced as `data_*_aggregated`), but can be traced by looking at lower levels of aggregation.

Aggregation is based only on complete data series and incomplete series are coded as missing values even if their incompleteness is minor. Where possible, at some point, incomplete series will be imputed to make them complete.

Consolidated datasets are not padded, ie. time points for which no indicator value exists are excluded.

### Monthly dataset

File: `data_consolidated_monthly.rds`. 

Variables:

* `date` (format: Y-m-d)
* water level: `h`, `h_min`, `h_max` (units: m a.s.l.)
* precipitation: `p` (units: mm)
* duration of precipitation: `pdur` (units: minutes)
* extraction: `ex` (units: 10^3 m3)
* evaporation: `e` (units: mm)
* water surface temperature: `wst` (units: °C)
* air temperature: `at` (units: °C)
* air pressure: `ap` (units: hPa)
* air humidity: `hum` (units: %)
* total solar irradiance: `sir` (units: J/cm2)
* (modeled) air temperature at 2m: `cds_t2m` (units: °C)
* (modeled) total evaporation: `cds_e` (units: m of water equivalent)

#### Water level (h)

Sources:

* `ozanic_monthly_h_1928-2003` (from 1928-05 to 1928-12)
* `ozanic_monthly_h_1929-2017` (from 1929-01 to 2017-12)
* `data_daily_aggregated` (from 2018-01 to 2024-09)

![](fig/cs_monthly_h.png)

#### Precipitation (p)

Sources:

* `ozanic_monthly_p_1926-2017` (from 1926-01 to 2017-12)

![](fig/cs_monthly_p.png)

#### Extraction (ex)

Sources:

* `ozanic_transc_ex_1967-1995` (from 1967-01 to 1995-12)
* `data_daily_aggregated` (from 2010-01 to 2024-10)

![](fig/cs_monthly_ex.png)

#### Evaporation (e)

Sources:

* `ozanic_transc_e_1977-1995` (from 1977-05 to 1995-12)
* `data_daily_aggregated` (from 2019-01 to 2013-12)

Notes:

* Data from two sources seems inconsistent, probably due to the fact that evaporation was measured on different locations.

![](fig/cs_monthly_e.png)

#### Water surface temperature (wst)

Sources:

* `ozanic_transc_wst_1979-1995` (from 1979-01 to 1995-12)

![](fig/cs_monthly_wst.png)

#### Air temperature (at)

Sources:

* `ozanic_transc_at_1981-1995` (from 1981-01 to 1995-12)
* `data_daily_aggregated` (from 2019-01 to 2013-12)

![](fig/cs_monthly_at.png)

#### Air pressure (ap)

Sources:

* `data_daily_aggregated` (from 2019-01 to 2023-12)

![](fig/cs_monthly_ap.png)

#### Air humidity (hum)

Sources:

* `ozanic_transc_hum_1981-1995` (from 1981-01 to 1995-12)
* `data_daily_aggregated` (from 2019-01 to 2023-12)

![](fig/cs_monthly_hum.png)

#### Solar irradiance (sir)

Sources:

* `data_daily_aggregated` (from 2019-01 to 2023-12)

![](fig/cs_monthly_sir.png)

#### (Modelled) air temperature (cds_t2m)

Sources:

* `Copernicus_CDS_monthly_t2m` (from 1950-01 to 2022-11)

#### (Modelled) evaporation (cds_e)

Sources:

* `Copernicus_CDS_monthly_e` (from 1950-01 to 2022-11)


### Daily dataset

File: `data_daily.rds`.

Variables:

* `date` (format: Y-m-d)
* water level: `h` (units: m a.s.l.)
* precipitation: `p` (units: mm)
* duration of precipitation: `pdur` (units: minutes)
* extraction: `ex` (units: 10^3 m3)
* evaporation: `e` (units: mm)
* water surface temperature: `wst` (units: °C)
* air temperature: `at` (units: °C)
* air pressure: `ap` (units: hPa)
* air humidity: `hum` (units: %)
* total solar irradiance: `sir` (units: J/cm2)

#### Water level (h)

Sources:

* `hvode_daily_h_1978-2022` (from 1978-01-01 to 2011-02-11, 2011-07-27, 2016-07-31, 2017-07-31)
* `viocl_daily_p_2011-Oct2024` (from 2011-01-01 to 2024-10-27)

![](fig/cs_daily_h.png)

#### Precipitation (p)

Sources:

* `ozanic_daily_p_1995-2000` (from 1995-01-01 to 2000-12-31)
* `ozanic_daily_p_2004-Aug2005` (from 2004-01-01 to 2005-08-31)
* `viocl_daily_p_2011-Oct2024` (from 2011-01-01 to 2024-12-31)

Note: zero values untill the end of last year!!! FIX?

![](fig/cs_daily_p.png)

#### Extraction (ex)

Sources:

* `viocl_daily_ex_2010-Oct2024` (from 2010-01-01 to 2024-10-27)

![](fig/cs_daily_ex.png)

#### Water surface temperatures (wst)

Sources:

* `hvode_daily_wst_1979-2022` (from 1979-01-01 to 2022-12-31)

![](fig/cs_daily_wst.png)

#### Air temperature (at)

Sources:

* `data_hourly_aggregated` (from 2019-01-01 to 2023-12-31)

![](fig/cs_daily_at.png)

#### Air pressure (ap)

Sources:

* `data_hourly_aggregated` (from 2019-01-01 to 2023-12-31)

![](fig/cs_daily_ap.png)

#### Air humidity (hum)

Sources:

* `data_hourly_aggregated` (from 2019-01-01 to 2023-12-31)

![](fig/cs_daily_hum.png)

#### Solar irradiance (sir)

Sources:

* `data_hourly_aggregated` (from 2019-01-01 to 2023-12-31)

![](fig/cs_daily_sir.png)



### Hourly dataset

File: `data_consolidated_hourly.rds`. 

Variables:

* time: `datetime`
* precipitation: `p` (units: mm)
* duration of precipitation: `pdur` (units: minutes)
* evaporation: `e` (units: mm)
* air temperature: `at` (units: °C)
* air pressure: `ap` (units: hPa)
* air humidity: `hum` (units: %)
* total solar irradiance: `sir` (units: J/cm2)

#### Precipitation (p)

Sources:

* `hvode_hourly_p_2019-2023` (from 2019-01-01 to 2023-12-31)

![](fig/cs_hourly_p.png)

#### Duration of precipitation (pdur)

Sources:

* `hvode_hourly_pdur_2019-2023` (from 2019-01-01 to 2023-12-31)

![](fig/cs_hourly_pdur.png)

#### Evaporation (e)

Sources:

* `hvode_hourly_e_2019-2023` (from 2019-01-01 to 2023-12-31)

![](fig/cs_hourly_e.png)

#### Air temperature (at)

Sources:

* `hvode_hourly_at_2019-2023` (from 2019-01-01 to 2023-12-31)

![](fig/cs_hourly_at.png)

#### Air pressure (ap)

Sources:

* `hvode_hourly_ap_2019-2023` (from 2019-01-01 to 2023-12-31)

![](fig/cs_hourly_ap.png)

#### Air humidity (hum)

Sources:

* `hvode_hourly_hum_2019-2023` (from 2019-01-01 to 2023-12-31)

![](fig/cs_hourly_hum.png)

#### Solar irradiance (sir)

Sources:

* `hvode_hourly_sir_2019-2023` (from 2019-01-01 to 2023-12-31)

![](fig/cs_hourly_sir.png)

## Data sources

### Hrvatske vode

National water management company.

#### Extraction, monthly series from 2014 to 2024

File: `hvode_monthly_h_2014-2024.rds`

Variables:

* `date` (format: Y-m-d)
* extraction: `ex` (units: 10^3 m3)

Notes:

* Values originally in m3, converted to 10^ m3.

#### Water level, daily series from 1978 to 2022

File: `hvode_daily_h_1978-2022.rds`

Variables:

* `date` (format: Y-m-d)
* water level: `h` (units: m a.s.l.)

Notes:

* Values originally in cm, here converted to m.
* Raw values recalculated to reference points.

#### Water surface temperature, daily series from 1979 to 2022

File: `hvode_daily_wst_1979-2022.rds`

Variables:

* `date` (format: Y-m-d)
* water surface temperature: `wst` (units: °C)

Notes:

* Missing values for year 2021.

#### Precipitation, hourly series from 2019 to 2023

File: `hvode_hourly_p_2019-2023.rds`

Variables:

* time: `datetime`
* precipitation: `p` (units: mm)

Notes:

* Data provided by DHMZ.
* Missing values due to (a) interruption of measurement originally coded as `/` and (b) incomplete data originally coded as `*` here converted to `NA`.

#### Precipitation duration, hourly series from 2019 to 2023

File: `hvode_hourly_pdur_2019-2023.rds`

Variables:

* time: `datetime`
* precipitation: `pdur` (units: minutes)

Notes:

* Data provided by DHMZ.
* Missing values due to (a) interruption of measurement originally coded as `/` and (b) incomplete data originally coded as `*` here converted to `NA`.

#### Evaporation, hourly series from 2019 to 2023

File: `hvode_hourly_e_2019-2023.rds`

Variables:

* time: `datetime`
* evaporation: `e` (units: mm)

Notes:

* Data provided by DHMZ.
* Extreme values (`e > 1`) removed.

#### Air temperature, hourly series from 2019 to 2023

File: `hvode_hourly_at_2019-2023.rds`

Variables:

* time: `datetime`
* air temperature: `at` (units: °C)

Notes:

* Data provided by DHMZ.

#### Air pressure, hourly series from 2019 to 2023

File: `hvode_hourly_ap_2019-2023.rds`

Variables:

* time: `datetime`
* air pressure: `ap` (units: %)

Notes:

* Data provided by DHMZ.

#### Air humidity, hourly series from 2019 to 2023

File: `hvode_hourly_hum_2019-2023.rds`

Variables:

* time: `datetime`
* air humidity: `hum` (units: %)

Notes:

* Data provided by DHMZ.

#### Solar irradiance, hourly series from 2019 to 2023

File: `hvode_hourly_sir_2019-2023.rds`

Variables:

* time: `datetime`
* solar irradiance: `sir` (units: J/cm2)

Notes:

* Data provided by DHMZ.
* Implicit missing values (night-time measurements) here set to zero, proper missing values (due to interruption of measurement or incomplete data) here coded as `NA`.


### VIOCL

Communal water managment company Vodoopskrba i odvodnja Cres Lošinj d.o.o.

#### Water level, daily series from 2011 to 2024

File: `VIOCL_daily_h_2011-2024.rds`.

Variables: 

* `date` (format: Y-m-d)
* water level: `h` (units: m a.s.l.)

Notes:

* Water level originally in cm, here converted to m.
* Water level values from Jan 1st to Feb 11th 2011 look inconsistent (values less than 400 cm, likely not corrected for reference value), here coded as missing.
* Record exist for an invalid date (Apr 31st 2011), here excluded from dataset.
* Missing values for Jul 27th 2011, Jul 31st 2016, Jul 31st 2017.

#### Precipitation, daily series from 2011 to 2024

File: `VIOCL_daily_p_2011-2024.rds`.

Variables: 

* `date` (format: Y-m-d)
* precipitation: `p` (units: mm)

Notes:

* Zero precipitation originally coded as missing values, here all converted to zero. Consequently, no missing values exist for precipitation.

#### Extraction, daily series from 2010 to 2024

File: `VIOCL_daily_ex_2010-2024.rds`.

Variables: 

* `date` (format: Y-m-d)
* extraction: `ex` (units: 10^3 m3)

Notes:

* Values originally in m3, here rescaled to 10^3 m3.
* Extreme and negative values removed.


### Ozanic

Nevenka Ožanić, private correspondence, transcription. Most historical data published in: Ožanić, N. (1996): Hidrološki model funkcioniranja Vranskog jezera na otoku Cresu. (Hydrological Functioning Model of the Vrana Lake on the Cres Island). Ph.d. thesis, Faculty of Civil Engineering – University of Split, Croatia.

#### Water level, monthly series from 1928 to 2003

File: `Ozanic_monthly_h_1928-2003.rds`

Variables:

* `date` (format: Y-m-d)
* water level: `h_mean`, `h_min`, `h_max` (units: m a.s.l.)

Notes:

* Series starts from May 1928.

#### Water level, monthly series from 1929 to 2017

File: `Ozanic_monthly_hmean_1929-2017.rds`

Variables:

* `date` (format: Y-m-d)
* water level: `h_mean` (units: m a.s.l.)

Notes:

* Multiple inconsistencies when compared with series from 1928 to 2003, especially in the period from 1942 to 1948 and from 1987 to 2003. Details available in `check_inconsistencies.R`, see also `fig/fig_inc_ozanic_hmean.png`.

#### Precipitation, monthly series from 1926 to 1997

File: `Ozanic_monthly_p_1926-1997.rds`

Variables:

* `date` (format: Y-m-d)
* precipitation: `p` (units: mm)

Notes:

* Precipitation levels originally recorded with lower precision (as integers) in period from 1926 to 1990 compared to period from 1991 to 1997. Integers here represented as real numbers up to one decimal place.


#### Precipitation, monthly series from 1926 to 2017

File: `Ozanic_monthly_p_1926-2017.rds`

Variables:

* `date` (format: Y-m-d)
* precipitation: `p` (units: mm)

Notes:

* Precipitation levels originally recorded with different precision (as integers) in period from 1926 to 1990 compared to period from 1991 to 2017. In this source file they are represented as real numbers up to one decimal place.

#### Evaporation, monthly series from 1977 to 1995

File: `Ozanic_transc_e_1977-1995.rds`

Variables:

* `date` (format: Y-m-d)
* evaporation: `e` (units: mm)

Notes:

* Evaporation measured partialy (only during summer) until 1991.
* Colmeans check mismatch for month of November.

#### Extraction, monthly series from 1977 to 1995

File: `Ozanic_transc_ex_1967-1995.rds`

Variables:

* `date` (format: Y-m-d)
* extraction: `ex` (units: 10^3 m3)

#### Air temperature, monthly series from 1981 to 1995

File: `Ozanic_transc_at_1981-1995.rds`

Variables:

* `date` (format: Y-m-d)
* extraction: `at` (units: °C)

#### Air humidity, monthly series from 1977 to 1995

File: `Ozanic_transc_hum_1981-1995.rds`

Variables:

* `date` (format: Y-m-d)
* extraction: `hum` (units: %)

Notes:

* Year 1983 missing from table, presumed to be a coding error (series starts in 1981).


#### Water surface temperature, monthly series from 1979 to 1995

File: `Ozanic_transc_wst_1977-1995.rds`

Variables:

* `date` (format: Y-m-d)
* extraction: `wst` (units: °C)

#### Precipitation, daily series from 1995 to 2000

Variables:

* `date` (format: Y-m-d)
* precipitation: `p` (units: mm)

Notes:

* When aggregated to monthly values, part of the series (from 1995 to 1997) is not matching with monthly series from 1926 to 2017. Details available in `check_inconsistencies.R`, see also `fig/fig_inc_ozanic_p.png`.

#### Precipitation, daily series from 2004 to Aug 2005

Variables:

* `date` (format: Y-m-d)
* precipitation: `p` (units: mm)

Notes:

* Month order in source tables is not clear, here assumed to be in sequential order from Jan to Dec.
* Record exist for an invalid date (Jun 31st 2004), here excluded from dataset.
* Zero precipitation originally coded as missing values, here converted to 0. Consequently, no missing values exist for precipitation.

### Copernicus CDS (Climate Data Store)

[ERA5-Land monthly averaged data from 1950 to present](https://cds.climate.copernicus.eu/cdsapp#!/dataset/reanalysis-era5-land-monthly-means), data for approximate coordinates of Vrana lake (44.84, 14.37).

Please consult CDS ERA5-Land documentation for detailed description of indicators and methodology.

#### Air temperature, monthly series

File: `CDS_monthly_t2m.rds`.

Variables:

* `date` (format: Y-m-d)
* air temperature at 2 m: `cds_t2m` (units: °C)

#### Total evaporation, monthly series

File: `CDS_monthly_e.rds`.

Variables:

* `date` (format: Y-m-d)
* total evaporation: `cds_e` (units: m of water equivalent)


![Monthly mean water level](fig/fig_h_mean_monthly.png)


