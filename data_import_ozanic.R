# Import from 'data/ozanic'

library(tidyverse)
library(readxl)

# Ozanic_monthly_h_1928-2003
# Water level, monthly (mean, min, max), series from 1926 to 2003

## h
data_h_mean <-
    read_xls("data/ozanic/Vransko jezero - mj nizovi vodostaja do 2003.xls", sheet = "SR", range = "a5:m81") |>
    setNames(c("year", formatC(seq(1:12), width = 2, flag = "0"))) |>
    pivot_longer(-year, names_to = "month", values_to = "h")

## h_min
data_h_min <-
    read_xls("data/ozanic/Vransko jezero - mj nizovi vodostaja do 2003.xls", sheet = "Min", range = "a5:m81") |>
    setNames(c("year", formatC(seq(1:12), width = 2, flag = "0"))) |>
    pivot_longer(-year, names_to = "month", values_to = "h_min")

## h_max
data_h_max <-
    read_xls("data/ozanic/Vransko jezero - mj nizovi vodostaja do 2003.xls", sheet = "Max", range = "a5:m81") |>
    setNames(c("year", formatC(seq(1:12), width = 2, flag = "0"))) |>
    pivot_longer(-year, names_to = "month", values_to = "h_max")

## merge
data_h_monthly_1928_2003 <-
    left_join(data_h_mean, data_h_min) |>
    left_join(data_h_max) |>
    mutate(date = as.Date(paste(year, month, "01", sep = "-"))) |>
    select(date,  h, h_min, h_max) |>
    pivot_longer(-date, names_to = "indicator") |>
    mutate(
        value = round(value, 2),
        data_source =
            case_match(
                indicator,
                "h" ~ "ozanic_monthly_h_1928-2003",
                "h_min" ~ "ozanic_monthly_hmin_1928-2003",
                "h_max" ~ "ozanic_monthly_hmin_1928-2003"
            )
    ) |>
    drop_na() |>
    arrange(indicator, date) |>
    select(date, data_source, indicator, value)

saveRDS(data_h_monthly_1928_2003, "data/ozanic_monthly_h_1928-2003.rds")


# Water level, monthly (h), series from 1929 to 2017

data_h_monthly_1929_2017 <-
    read_tsv("data/ozanic/Ozanic_monthly_h_mean.tab") |>
    select(1:13) |>
    setNames(c("year", formatC(seq(1:12), width = 2, flag = "0"))) |>
    pivot_longer(-year, names_to = "month", values_to = "h") |>
    mutate(date = as.Date(paste(year, month, "01", sep = "-"))) |>
    select(date, h) |>
    pivot_longer(h, names_to = "indicator") |>
    mutate(
        data_source = "ozanic_monthly_h_1929-2017",
        value = round(value, 2)
    ) |>
    drop_na() |>
    arrange(indicator, date) |>
    select(date, data_source, indicator, value)

saveRDS(data_h_monthly_1929_2017, "data/ozanic_monthly_h_1929-2017.rds")

# Precipitation, monthly, from 1926 to 1997

data_p_monthly_1926_1997 <-
    read_xls("data/ozanic/Vransko- oborine.xls", sheet = "mjes ob 26-97", range = "a1:m73") |>
    setNames(c("year", formatC(seq(1:12), width = 2, flag = "0"))) |>
    pivot_longer(-year, names_to = "month", values_to = "p") |>
    mutate(date = as.Date(paste(year, month, "01", sep = "-"))) |>
    select(date, p) |>
    pivot_longer(p, names_to = "indicator") |>
    mutate(
        value = round(value,1),
        data_source = "ozanic_monthly_p_1926-1997"
    ) |>
    arrange(indicator, date) |>
    select(date, data_source, indicator, value)

saveRDS(data_p_monthly_1926_1997, "data/ozanic_monthly_p_1926-1997.rds")

# Precipitation, monthly, from 1926 to 2017

data_p_monthly_1926_2017 <-
    read_tsv("data/ozanic/Ozanic_monthly_p.tab") |>
    select(1:13) |>
    setNames(c("year", formatC(seq(1:12), width = 2, flag = "0"))) |>
    pivot_longer(-year, names_to = "month", values_to = "p") |>
    mutate(date = as.Date(paste(year, month, "01", sep = "-"))) |>
    select(date, p) |>
    pivot_longer(p, names_to = "indicator") |>
    mutate(
        value = round(value,1),
        data_source = "ozanic_monthly_p_1926-2017"
    ) |>
    arrange(indicator, date) |>
    select(date, data_source, indicator, value)

saveRDS(data_p_monthly_1926_2017, "data/ozanic_monthly_p_1926-2017.rds")


# Precipitation, daily
# (a) Part of the daily series 1995-2000 is not matching with monthly data.
# (b) Month order not clear in series 2004-2005, invalid date values.

## Series from 1995 to 2000

data_p_daily_1 <-
    read_xls("data/ozanic/Vransko- oborine.xls", sheet = "dnevne 95-2000") |>
    transmute(
        date = as.Date(DATUM),
        p = round(`CP Vrana - dnevne oborine`,1),
        data_source = "ozanic_daily_p_1995-2000"
    ) |>
    mutate(p = replace_na(p, 0)) |>
    pivot_longer(p, names_to = "indicator")

saveRDS(data_p_daily_1, "data/ozanic_daily_p_1995-2000.rds")

## (mismatch with monthly data)
#data_p_daily_1 |>
#    mutate(
#        year = format(date, "%Y"),
#        month = format(date, "%m")
#    ) |>
#    group_by(year, month) |>
#    summarize(p = sum(p)) |>
#    mutate(data_source = "aggregated") |>
#    full_join(data_p_monthly_1926_2017 |> mutate(data_source = "monthly")) |>
#    mutate(date = as.Date(paste(year, month, "01", sep = "-"))) |>
#    filter(date >= "1995-01-01" & date < "1998-01-01") |>
#    ggplot(aes(x = date, y = p, color = data_source)) +
#    geom_point()


## Series from 2004 to August 2005
## (month order in tables is not clear)
## (invalid values, eg. Jun 31 2004, such date doesn't exist)

data_p_daily_2a <-
    read_xls("data/ozanic/Vransko- oborine.xls", sheet = "dnevne 04 i pola 05", range = "a3:m34") |>
    setNames(c("dom", formatC(seq(1:12), width = 2, flag = "0"))) |> #! Dec = Jan?
    pivot_longer(-dom, names_to = "month", values_to = "p") |>
    mutate(date = as.Date(paste("2004", month, dom, sep = "-"))) |>
    select(date, p)

data_p_daily_2b <-
    read_xls("data/ozanic/Vransko- oborine.xls", sheet = "dnevne 04 i pola 05", range = "a45:i76") |>
    setNames(c("dom", formatC(seq(1:8), width = 2, flag = "0"))) |> #! Dec = Jan?
    pivot_longer(-dom, names_to = "month", values_to = "p") |>
    mutate(date = as.Date(paste("2005", month, dom, sep = "-"))) |>
    select(date, p)

## merge series 2004, 2005
data_p_daily_2 <-
    bind_rows(data_p_daily_2a, data_p_daily_2b) |>
    mutate(
        p = replace_na(round(p,1), 0),
        data_source = "ozanic_daily_p_2004-Aug2005"
    )

## (invalid value!)
data_p_daily_2 |>
    filter(is.na(date))

## cleanup & save
data_p_daily_2 |>
    filter(!is.na(date)) |>
    pivot_longer(p, names_to = "indicator") |>
    saveRDS("data/ozanic_daily_p_2004-Aug2005.rds")


# Import transcribed data

source("data/ozanic/thesis_transcription_e.R")
source("data/ozanic/thesis_transcription_ex.R")
source("data/ozanic/thesis_transcription_at.R")
source("data/ozanic/thesis_transcription_hum.R")
source("data/ozanic/thesis_transcription_wst.R")
