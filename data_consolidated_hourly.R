# Consolidate data on hourly level
library(tidyverse)
theme_set(theme_bw())

## Read & merge hourly datasets

data_hourly_full <- c(
    "data/hvode_hourly_p_2019-2023.rds",
    "data/hvode_hourly_pdur_2019-2023.rds",
    "data/hvode_hourly_e_2019-2023.rds",
    "data/hvode_hourly_at_2019-2023.rds",
    "data/hvode_hourly_ap_2019-2023.rds",
    "data/hvode_hourly_hum_2019-2023.rds",
    "data/hvode_hourly_sir_2019-2023.rds"
) |>
    map(readRDS) |>
    list_rbind() |>
    arrange(data_source, datetime)

saveRDS(data_hourly_full, "data/data_hourly_full.rds")

## Consolidate

data_hourly_full |>
    distinct(data_source, indicator)

### (p)

data_p <-
    data_hourly_full |>
    filter(
    (data_source %in% "hvode_hourly_p_2019-2023")
    )

### (pdur)

data_pdur <-
    data_hourly_full |>
    filter(
    (data_source %in% "hvode_hourly_pdur_2019-2023")
    )

### (e)

data_e <-
    data_hourly_full |>
    filter(
    (data_source %in% "hvode_hourly_e_2019-2023")
    )

### (at)

data_at <-
    data_hourly_full |>
    filter(
    (data_source %in% "hvode_hourly_at_2019-2023")
    )

### (ap)

data_ap <-
    data_hourly_full |>
    filter(
    (data_source %in% "hvode_hourly_ap_2019-2023")
    )

### (hum)

data_hum <-
    data_hourly_full |>
    filter(
    (data_source %in% "hvode_hourly_hum_2019-2023")
    )

### (sir)

data_sir <-
    data_hourly_full |>
    filter(
    (data_source %in% "hvode_hourly_sir_2019-2023")
    )


## Save datasets

data_hourly_sourced <- list(
    data_p,
    data_pdur,
    data_e,
    data_at,
    data_ap,
    data_hum,
    data_sir
) |>
    list_rbind()

saveRDS(data_hourly_sourced, "data/data_hourly_sourced.rds")
write_csv(data_hourly_sourced, "data/data_hourly_sourced.csv")

data_hourly <-
    data_hourly_sourced |>
    pivot_wider(id_cols = datetime, names_from = indicator) |>
    arrange(datetime)

saveRDS(data_hourly, "data/data_hourly.rds")
write_csv(data_hourly, "data/data_hourly.csv")

